import { BloomMetadata, Maybe } from './models_gen';

export * from './models_gen';

export type DashboardData = {
  metadata: Maybe<BloomMetadata>,
};
