module gitlab.com/bloom42/bloom/core

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	gitlab.com/bloom42/bloom/common v0.0.0-20200309140008-9ba51c024c2e
	gitlab.com/bloom42/libs/graphql-go v0.0.0-20200302110054-1cc05f095d21
	gitlab.com/bloom42/libs/rz-go v1.3.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
