package consts

const KDF_PW_KEYLEN uint64 = 32
const KDF_AUTH_CONTEXT = "blm_auth"
const KDF_AUTH_ID uint64 = 1
const KDF_AUTH_KEYLEN uint64 = 64
