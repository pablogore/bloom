export default {
  labels: {
    other_downloads: 'Tous les Téléchargements',
    learn_more: 'Plus d\'info',
    coming_soon_btn: 'Sortie début 2020',
  },
  appbar: {
    download: 'Télécharger',
    features: 'Fonctionnalités',
  },
  landing: {
    mission: 'Nous pensons que chacun a droit au respect et à la vie privée',
    subtitle2: '<b>Reprenez le contrôle avec <span class="blm-rounded-elegance"><b>Bloom</b></span></b>',
    explore_features: 'Découvrez les fonctionnalités',
    feature1: {
      // title: 'Fast, Simple, Secure',
      title: 'Un lieu sûr pour toutes vos données',
      description: `En tant que projet Open Source, n'importe qui peut regarder comment fonctionne
      Bloom. Nous utilisons l'état de l'art en cryptographie pour garder vos données en sécurité.
      Il n'y a pas de publicité, pas de marketing d'affiliation, pas de trackers envahissants. <br />
      Seulement de la technologie ouverte pour une expérience rapide, simple et sécure.`,
    },
    feature2: {
      title: 'Un outil collaboratif',
      description: `Que ce soit avec vos amis ou vos collègues, Bloom a été pensé pour la coopération.
      Organisez vos fichiers, partagez vos idées et organisez votre vie.
      Enfin des outils pensés pour passer moins de temps devant un écran, et plus dans la vraie vie.<br />
      N'arrêtez jamais le fun.`,
    },
  },
  p404: {
    not_found: 'Page non trouvée',
    back_home: 'Retour à l\'accueil',
  },
};
